import Home from "./pages/Home";
import GlobalStyles from "./styles/GlobalStyles";
import Footer from "./components/Footer";
import Header from "./components/Header";

function App() {
  return (
    <>
      <GlobalStyles />
      <Header/>
      <Home />
      <Footer/>
    </>
  );
}

export default App;
