type InfoWall = {
    width: number;
    height: number;
    quantityDoor: number;
    quantityWindow: number;
    finalArea?: number;
    errorsMsgs?: string[];
    isOk?: boolean
}

export default InfoWall;