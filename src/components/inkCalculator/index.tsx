import { useState } from "react";
import { Container, InkCalculatorTop,  ModalHeader, ModalClose, Content, ModalText, ModalMenu, ModalMenuItem} from './styles';  
import Measurements from '../Measurements' 
import InfoWall from "../../types/infoWall";


interface IInkProps {
    isCloseModal: (value: boolean)=> void;
  }



const Header: React.FC<IInkProps> = ({isCloseModal}:IInkProps) => {

    const [itemSelect, setItemSelect] = useState(0);

    

    const [infosWall, setInfosWall] = useState<InfoWall[]>([
        {
            width: 0,
            height: 0,
            quantityDoor: 0,
            quantityWindow: 0,         
            finalArea: 0,
            errorsMsgs: [],
            isOk: false
        },
        {
            width: 0,
            height: 0,
            quantityDoor: 0,
            quantityWindow: 0,       
            finalArea: 0,
            errorsMsgs: [],
            isOk: false
        },
        {
            width: 0,
            height: 0,
            quantityDoor: 0,
            quantityWindow: 0,         
            finalArea: 0,
            errorsMsgs: [],
            isOk: false
        },
        {
            width: 0,
            height: 0,
            quantityDoor: 0,
            quantityWindow: 0,         
            finalArea: 0,
            errorsMsgs: [],
            isOk: false
        },
    ])




      const addValueWall = (newInfoWall: InfoWall, itemSelect: number) => {
        infosWall[itemSelect] = newInfoWall;
        setInfosWall(infosWall);
        if( itemSelect < 3  && newInfoWall.errorsMsgs?.length === 0 ) setItemSelect(itemSelect + 1)

      }






 
    return (
      <Container>
        <InkCalculatorTop>
            <ModalHeader>Calculadora de Tintas</ModalHeader>
            <ModalClose onClick={()=> isCloseModal(false)}>x</ModalClose>
        </InkCalculatorTop>
        <Content>
            <ModalText>Considerando que a sala é sala é composta de 4 paredes, peço que informe os dados baixo:</ModalText>
            <ModalMenu>
                <ModalMenuItem  onClick={() =>setItemSelect(0) }  className={`${infosWall[0].isOk ? "correct" : ""} ${infosWall[0].errorsMsgs && infosWall[0].errorsMsgs.length >= 1  ? "error": ""} ${itemSelect === 0  ? "select" : ""}`} >
                    <p> Parede 1</p>

                </ModalMenuItem>
                <ModalMenuItem onClick={() => setItemSelect(1) }  className={`${infosWall[1].isOk ? "correct" : ""} ${infosWall[1].errorsMsgs && infosWall[1].errorsMsgs.length >= 1  ? "error": ""} ${itemSelect === 1  ? "select" : ""}`}>
                    <p>
                        Parede 2
                    </p>
                </ModalMenuItem>
                <ModalMenuItem onClick={() =>setItemSelect(2) }  className={`${infosWall[2].isOk ? "correct" : ""} ${infosWall[2].errorsMsgs && infosWall[2].errorsMsgs.length >= 1  ? "error": ""} ${itemSelect === 2  ? "select" : ""}`}>
                    <p> 
                    Parede 3  
                    </p></ModalMenuItem>
                <ModalMenuItem onClick={() =>setItemSelect(3) } className={`${infosWall[3].isOk ? "correct" : ""} ${infosWall[3].errorsMsgs && infosWall[3].errorsMsgs.length >= 1  ? "error": ""} ${itemSelect === 3  ? "select" : ""}`}>
                    <p>
                        Parede 4
                    </p>
                    </ModalMenuItem>
            </ModalMenu>
            <div>
                <Measurements infosWall={infosWall} itemSelect={itemSelect} addNewValue={ addValueWall}/>


            </div>
            

            
        </Content>

    
         
     
        
      </Container>
    );
  };

  export default Header