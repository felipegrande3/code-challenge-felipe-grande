import styled from "styled-components";

 export const Container = styled.div`
    max-width:600px;
    width: 100%


 `;
 export const Content = styled.div`
    margin-top: 1rem;
    
 `;
 export const ModalText = styled.p`
   color:#12171C;
   font-weight: 400;
   font-size: 14px
    
    
 `;
export const ModalMenu = styled.div`
    display: grid;
    margin: 1rem 0;
    width: 100%;
    grid-template-columns: repeat(4, 1fr);

    .select{
        border: 1px solid #003D7C;
        color: #003D7C

    }
    .select p{
        color: #003D7C;
        font-weight: 700

    }
    .correct{
        border: 1px solid #63A355 ;
        color: #63A355 ;
    }
    .correct p{
        color: #63A355 ;
        font-weight: 700 ;
    }
    .error{
        border: 1px solid #E03C31 !important;
        color: #E03C31 !important;
    }
    .error p{
        color: #E03C31 !important;
        font-weight: 700 !important;
    }



`;
export const ModalMenuItem = styled.div`
    cursor: pointer;
    margin: 0 auto;
    border: 1px solid #E8E8E8;
    border-radius: 50px;
    padding: 0.3rem 0.8rem;

    .select{
        border: 1px solid #003D7C;
        color: #003D7C

    }

    p{
        color: #808080;
        font-size: 13px;
        font-weight: 400
    }

`;


 export const InkCalculatorTop = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center


 `
 export const ModalHeader = styled.p`
   color: #12171C;
   font-size: 16px;
   font-weight: 700


 `
 export const ModalClose = styled.p`
 font-size: 14px;
 color: #dcdcdc;
 cursor: pointer
   

 `
