import {
  Container,
  BoxCalculatorTitle,
  BoxCalculatorText,
  BoxCalculatorButton,
} from "./styles";
import { useState } from "react";
import Modal from "../Modal";
import Icon from "../../assets/img/icone.png";

const BoxCalculator: React.FC = () => {
  const [isOpenModal, setIsOpenModal] = useState(false);

  const openModal = () => {
    setIsOpenModal(true);
  };

  const closeModal = (isClose: boolean) => {
    setIsOpenModal(isClose);
  }



  return (
    <Container>
      <div>
        <img src={Icon} alt="icone " />
      </div>
      <div>
        <BoxCalculatorTitle>Calculadora de Tintas</BoxCalculatorTitle>
        <BoxCalculatorText>
          A pintura é uma parte importante de uma construção e quem está
          reformando sempre fica em dúvida sobre como calcular a quantidade
          ideal de latas de tintas . Desenvolvemos uma calculadora super prática
          para te ajudar nessa tarefa.
        </BoxCalculatorText>
        <BoxCalculatorButton onClick={openModal}>
          Abrir calculadora
        </BoxCalculatorButton>
        {isOpenModal &&
         <Modal
          isClose={closeModal}>
          
          </Modal>}
      </div>
    </Container>
  );
};

export default BoxCalculator;
