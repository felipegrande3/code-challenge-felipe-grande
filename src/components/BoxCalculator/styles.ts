import styled from "styled-components";

 export const Container = styled.div`
   max-width: 1400px;
    justify-content: center;
   margin: 0 auto;
   padding: 2rem 0;
    display: flex;
    gap: 2rem     
 `;

 export const BoxCalculatorTitle = styled.h1`
    font-size: 20px;
    font-weight: 700;
    color: #003D7C;
    margin-bottom: 0.5rem
 `
 export const BoxCalculatorText = styled.p`
    font-size: 14px;
    font-weight: 500;
    color: #12171C;
    width: 450px;
 
 `
 export const BoxCalculatorButton = styled.button`
    background-color: #184077;
    margin: 1rem 0;
    color:  #fff;
    font-size: 12px;
    padding: 0.5rem 1rem;
    border-radius: 4px;
 
 `