import { Container } from './styles';   
const Footer: React.FC = () => {
 
    return (
      <Container>
        <p>&copy; Copyright Calculadora de Tintas - 2022</p>
        
      </Container>
    );
  };

  export default Footer