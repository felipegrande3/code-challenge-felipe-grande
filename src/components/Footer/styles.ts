import styled from "styled-components";

 export const Container = styled.div`
    background-image: linear-gradient(to right, #004AAD ,#202D6A);

    p{
        color: #fff;
        padding: 1rem;
        text-align: center;
        font-size: 13px;
        font-weight: 400
    }

 `;