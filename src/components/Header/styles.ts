import styled from "styled-components";

 export const Container = styled.div`
   background-image: linear-gradient(to right, #004AAD ,#202D6A);
   display: flex

  
 `;
 export const Content = styled.div`
   display: flex;
   flex-direction: column;
   padding: 1rem;
   margin: 0 auto;
  
 `;
 export const Logo = styled.div`
   display: flex

  
 `;
 export const LogoDigital = styled.p`
   background: #f58614;
   color: black;
   font-weight: 700;
   font-size: 20px

  
 `;
 export const LogoRepublic = styled.p`
   background: #f1471f;
   color: black;
   font-weight: 700;
   font-size: 20px
  
 `;
