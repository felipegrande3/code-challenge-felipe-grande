
import { Container,  Content } from './styles';   
import InkCalculator from "../inkCalculator"
interface IMoldalProps {
    children: any,
    isClose: (value: boolean)=> void
}


const Modal: React.FC<IMoldalProps> = ({children, isClose}: IMoldalProps) => {

    

    const closeModal = (isCloseModal: boolean) => {
      isClose(isCloseModal)
    }



    return (
      <Container>
        <Content>
          <InkCalculator
            isCloseModal={closeModal} />
          {children}
        </Content>
      </Container>
    );
  };

  export default Modal