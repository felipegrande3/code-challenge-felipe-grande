import styled from "styled-components";

 export const Container = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: rgba(8, 65, 84, 0.25);
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 9999;

 `;
 export const Content = styled.div`
   position: absolute;
    background-color: #F9F9F9;
    padding:  1rem;
    border-radius: 10px;
    max-width: 450px;
    width: 90%
 `;



