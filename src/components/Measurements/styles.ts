import styled from "styled-components";

export const Container = styled.form``;
export const GridArea = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 0.5rem;
`;
export const InputDadosTitle = styled.p`
  margin: 0.5rem 0;
  color: #12171c;
  font-weight: 400;
  font-size: 13px;
`;
export const AlertErro = styled.p`
  color: #fd5e5e;
  font-size: 12px;
  font-weight: 500;
`;
export const InputDados = styled.input`
  background-color: #f5f5f5;
  border-radius: 5px;
  border: 1px solid #e8e8e8;
  padding: 0.3rem;
  font-size: 13px;
  width: 100%;
`;

export const ModalButton = styled.button`
  background-color: #184077;
  border: 2px solid  #184077;
  margin: 1.5rem 0;
  color: #fff;
  font-weight: 700;
  font-size: 14px;
  padding: 0.5rem 2rem;
  border-radius: 4px;

  :hover{
   background-color: #fff;
   color: #184077;
  }
`;
export const Send = styled.div`
  display: flex;
  justify-content: center;
`;
export const Big = styled.img`
  width: 40px;
`;
export const Medium = styled.img`
  width: 30px;
`;
export const Small = styled.img`
  width: 25px;
`;
export const Tiny = styled.img`
  width: 20px;
`;
export const ResultContainer = styled.div`
  margin: 2rem 0;
  display: flex;
  gap: 1rem;
  align-items: baseline;
`;
export const ResultText = styled.p`
  color: #222755;
  font-weight: 500;
  font-size: 14px;
`;
export const Quantity = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
