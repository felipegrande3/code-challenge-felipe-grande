import { useState, useEffect } from "react";
import InfoWall from "../../types/infoWall";
import painting from "../../assets/img/painting.png";

import {
  Container,
  GridArea,
  InputDados,
  InputDadosTitle,
  AlertErro,
  ModalButton,
  Send,
  Big,
  Medium,
  Small,
  Tiny,
  ResultContainer,
  ResultText,
  Quantity,
} from "./styles";

interface IMeasurementsProps {
  volumeALL?: any;
  itemSelect: number;
  infosWall: InfoWall[];
  addNewValue: (newInfoWall: InfoWall, item: number) => void;
}

const Measurements: React.FC<IMeasurementsProps> = ({
  volumeALL,
  itemSelect,
  infosWall,
  addNewValue,
}: IMeasurementsProps) => {
  const infosForm = infosWall[itemSelect];

  const {
    width: widthProps,
    height: heightProps,
    quantityDoor,
    quantityWindow,
    finalArea,
    errorsMsgs,
  } = infosForm;

  const [height, setHeight] = useState(heightProps);
  const [width, setWidth] = useState(widthProps);
  const [door, setDoor] = useState(quantityDoor);
  const [window, setWindow] = useState(quantityWindow);
  const [calculate, setCalculate] = useState(false);
  const [erroMsg, setErroMsg] = useState<any>("");
  const [quantity, setQuantity] = useState([]);

  let windonMeter = 2.0 * 1.2;
  let doorMeter = 0.8 * 1.9;
  let meterAdditional = 0;
  const paintCans: any = [];

  useEffect(() => {
    setHeight(heightProps);
    setWidth(widthProps);
    setWindow(quantityWindow);
    setDoor(quantityDoor);
  }, [
    heightProps,
    widthProps,
    quantityWindow,
    quantityDoor,
    finalArea,
    itemSelect,
  ]);

  const validationMeter = (area: number) => {
    if (area > 50) return "A parede deverá ter no maximo 50m.²";
    if (area < 1) return "A parede deverá ter no minimo 1m.²";

    return "";
  };

  const validateDoorHeight = (door: number, heigth: number) => {
    if (door >= 1 && heigth < 2.2)
      return "Em ambientes com porta a altura minima deve ser 2,20m.";

    return "";
  };

  const areaWindonDoor = (
    door: number,
    windon: number,
    mtDoor: number,
    mtWindon: number
  ) => {
    let meter = door * mtDoor + windon * mtWindon;
    return meter;
  };

  const isFormValid = (item: InfoWall) => {
    let msgsError = [];
    let area = item.height * item.width;

    let isValidaMeter = validationMeter(area);
    if (isValidaMeter !== "") msgsError.push(isValidaMeter);
    let isValidaDoorHeigth = validateDoorHeight(door, item.height);
    if (isValidaDoorHeigth !== "") msgsError.push(isValidaDoorHeigth);
    if (door >= 1 || window >= 1)
      meterAdditional = areaWindonDoor(
        item.quantityDoor,
        item.quantityWindow,
        doorMeter,
        windonMeter
      );
    if (meterAdditional >= area / 2)
      msgsError.push(
        "A quantidade de Portas/Janelas acima do limite para o tamanho da parede "
      );

    return msgsError;
  };
  const isGoQuery = (a: any) => {
    for (var i = 0; i < a.length; i++) {
      if (a[i].finalArea === 0) {
        return false;
      }
    }
    return true;
  };

  const submitForm = (evt: React.FormEvent<HTMLFormElement>) => {
    evt.preventDefault();

    const newItem = {
      height,
      width,
      quantityDoor: door,
      quantityWindow: window,
    };

    //as validações
    let isValid = isFormValid(newItem);
    let areaFinal = height * width - meterAdditional;

    if (isValid.length === 0) {
      setErroMsg([]);
      addNewValue(
        { ...newItem, finalArea: areaFinal, errorsMsgs: [] , isOk: true},
        itemSelect
      );
      
      
    } else {
      addNewValue(
        { ...newItem, finalArea: 0, errorsMsgs: isValid },
        itemSelect
      );
      setErroMsg(isValid);
    }
    let result = isGoQuery(infosWall);
    if (result) setCalculate(true);
  };

  const calculateTotal = (area: number) => {

    let literPerMeter = area / 5;
   

    if (literPerMeter > 18) {
      let bigLiters = Math.floor(literPerMeter / 18);
      paintCans.push(bigLiters);
      let bigLitersRest = literPerMeter % 18;
      literPerMeter = bigLitersRest;
    } else {
      paintCans.push(0);
    }

    if (literPerMeter > 3.6) {
      let mediumLiter = Math.floor(literPerMeter / 3.6);
      paintCans.push(mediumLiter);
      let mediumLitersRest = literPerMeter % 3.6;
      literPerMeter = mediumLitersRest;
    } else {
      paintCans.push(0);
    }

    if (literPerMeter > 2.5) {
      let smallLiter = Math.floor(literPerMeter / 2.5);
      paintCans.push(smallLiter);
      let smallLitersRest = literPerMeter % 2.5;
      literPerMeter = smallLitersRest;
    } else {
      paintCans.push(0);
    }

    if (literPerMeter > 0.5) {
      let tinyLiter = Math.floor(literPerMeter / 0.5);
      let tinyLitersRest = literPerMeter % 0.5;
      literPerMeter = tinyLitersRest;
   
      if (tinyLitersRest !== 0) tinyLiter = tinyLiter + 1;
      paintCans.push(tinyLiter);
    } else {
      paintCans.push(0);
    }
  

    setQuantity(paintCans);
  
  };

  const sumArea = () => {
    let area = 0;
    for (var i = 0; i < infosWall.length; i++) {
      area = Number(infosWall[i].finalArea) + area;
    }

    calculateTotal(area);
  };

  return (
    <Container onSubmit={submitForm}>
      <GridArea>
        <div>
          <InputDadosTitle>Altura(m)</InputDadosTitle>
          <InputDados
            type="number"
            placeholder="0.0 m"
            disabled={quantity.length >= 1}
            value={height}
            onChange={(evt) => setHeight(Number(evt.target.value))}
          ></InputDados>
        </div>
        <div>
          <InputDadosTitle>Largura(m)</InputDadosTitle>
          <InputDados
            type="number"
            disabled={quantity.length >= 1}
            placeholder="0.0 m"
            value={width}
            onChange={(evt) => setWidth(Number(evt.target.value))}
          ></InputDados>
        </div>
      </GridArea>

      <GridArea>
        <div>
          <InputDadosTitle>Nº de Janelas(2,00 x 1,20m) </InputDadosTitle>
          <InputDados
            type="number"
            disabled={quantity.length >= 1}
            value={window}
            onChange={(evt) => setWindow(Number(evt.target.value))}
          ></InputDados>
        </div>
        <div>
          <InputDadosTitle>Nº de portas(0,80 x 1,90m) </InputDadosTitle>
          <InputDados
            type="number"
            value={door}
            disabled={quantity.length >= 1}
            onChange={(evt) => setDoor(Number(evt.target.value))}
          ></InputDados>
        </div>
      </GridArea>

      
      {erroMsg &&
        errorsMsgs?.map((e, index) => <AlertErro key={index}>{e}</AlertErro>)}
      {
        quantity.length === 0 ? (
          <Send>
            <ModalButton   type="submit">Salvar</ModalButton>
        </Send>
        ):(
          <Send>
            <ModalButton  onClick={() => setQuantity([])} >Refazer Calculo</ModalButton>
        </Send>
        )
      }
     


      {calculate && (
        <Send>
          <ModalButton onClick={sumArea}>Calcular a quantidade de Tinta</ModalButton>
        </Send>
      )
      }
      {quantity.length >= 1 && (
        <section>
          <ResultText>Para essa metragem será necessario:</ResultText>

          <ResultContainer>
            {quantity && quantity[0] >= 1 && (
              <Quantity>
                <div>
                  <Big src={painting} alt="icone " />
                </div>
                <ResultText>{quantity[0]} x 18L</ResultText>
              </Quantity>
            )}
            {quantity && quantity[1] >= 1 && (
              <Quantity>
                <div>
                  <Medium src={painting} alt="icone " />
                </div>
                <ResultText>{quantity[1]} x 3.6L</ResultText>
              </Quantity>
            )}
            {quantity && quantity[2] >= 1 && (
              <Quantity>
                <div>
                  <Small src={painting} alt="icone " />
                </div>
                <ResultText> {quantity[2]} x 2.5L</ResultText>
              </Quantity>
            )}
            {quantity && quantity[3] >= 1 && (
              <Quantity>
                <div>
                  <Tiny src={painting} alt="icone " />
                </div>
                <ResultText> {quantity[3]} x 0.5L</ResultText>
              </Quantity>
            )}
          </ResultContainer>
        </section>
      )}
    </Container>
  );
};

export default Measurements;
