import styled from "styled-components";

 export const Container = styled.div`
    background: #F0F0F0;
    flex-grow: 1
    

 `;
 export const Content = styled.div`
   


 `;
 export const Title = styled.h1`
   font-weight: 700;
   font-size: 24px;
   padding: 1rem;
   text-align: center;
   color: #003D7C

 `;
 export const SubTitle = styled.h1`
   font-weight: 500;
   font-size: 14px;
   text-align: center;
   color: #12171C

 `;
 export const BannerImg = styled.img`
    width: 100%;
    height:200px;
    
    @media (max-width: 768px) {
      height:auto;
  }
    


 `;