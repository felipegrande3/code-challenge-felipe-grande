import { Container, Content, BannerImg, Title, SubTitle } from "./styles";
import Banner from "../assets/img/banner_principal.jpg"
import BoxCalculator from "../components/BoxCalculator";

const Home: React.FC = () => {

  return (
    <Container>
      <Content>
        <BannerImg src={Banner} alt="imagem do banner principal"/>
        <Title>Calculadoras</Title>
        <SubTitle>Disponibilizamos calculadoras para você encontrar o produto ideal</SubTitle>
        <BoxCalculator/>
      </Content>
    </Container>
  );
};

export default Home;
