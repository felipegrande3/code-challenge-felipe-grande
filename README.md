**

# Calculadora de Tintas


Este projeto consiste em uma calculadora de litros de tintas em um ambiente, o usuario digita a metragem das paredes e calculadora retorna a quantidade de tinta necessaria para a pintura

## **Tecnologias utilizadas**

React  v18.2.0
yarn v1.22.19
Styled-components  v5.3.6

## **Passo a passo para rodar o projeto**

    git clone https://gitlab.com/felipegrande3/code-challenge-felipe-grande.git
    yarn
    yarn start

